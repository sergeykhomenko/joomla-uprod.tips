<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_utips
 *
 * @copyright   © 2017 Untitled Production. All Rights Reserved.
 * @license     GNU General Public License
 */

defined('_JEXEC') or die;
?>


<div class="utips_general utips_wrapper tip">
	<?php if ($params->get('fontellousing')) : ?>
		<span class="tip__icon <?php echo $params->get('icon_class'); ?>"></span>
	<?php endif; ?>
	<?php if ($params->get('showtitle')) : ?>
		<a href="<?php echo $params->get('readmore_link'); ?>">
			<h4 class="tip__title"><?php echo $params->get('title'); ?></h4>
		</a>
	<?php endif; ?>
	<p class="tip__content">
		<?php echo $params->get('content'); ?>
	</p>
	<a href="<?php echo $params->get('readmore_link'); ?>" class="button tip__more">
		<?php echo $params->get('readmore_text'); ?>
	</a>
</div>