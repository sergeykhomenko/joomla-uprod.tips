<?php
/**
 * @package     Joomla.Site
 * @subpackage  mod_utips
 *
 * @copyright   © 2017 Untitled Production. All Rights Reserved.
 * @license     GNU General Public License
 */
 
defined('_JEXEC') or die;

require JModuleHelper::getLayoutPath('mod_utips', 'default');