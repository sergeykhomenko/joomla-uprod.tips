# About module #
Show tip with custom title. Supports html, inline styles, fontello/font-awesome icons and multi-line text.
![Module view](http://untitled-production.com/files/joomla/mod_utips/utips%20img.jpg)

### What is this? ###
I made this module for one of my projects. You can use it on your website. It's free.

### How do i contact with author? ###
Contact me by e-mail: khomenko.mail@gmail.com

# How to use #

### How do I get set up? ###
1. Download mod_utips.zip
2. You have set up file. Use it in your joomla administration panel for install module on your website